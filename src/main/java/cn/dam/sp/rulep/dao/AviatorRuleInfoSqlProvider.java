package cn.dam.sp.rulep.dao;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import cn.dam.sp.rulep.bean.AviatorRuleInfo;
import cn.dam.sp.rulep.bean.AviatorRuleInfoExample.Criteria;
import cn.dam.sp.rulep.bean.AviatorRuleInfoExample.Criterion;
import cn.dam.sp.rulep.bean.AviatorRuleInfoExample;
import java.util.List;
import java.util.Map;

public class AviatorRuleInfoSqlProvider {

    public String countByExample(AviatorRuleInfoExample example) {
        BEGIN();
        SELECT("count(*)");
        FROM("AVIATOR_RULE_INFO");
        applyWhere(example, false);
        return SQL();
    }

    public String deleteByExample(AviatorRuleInfoExample example) {
        BEGIN();
        DELETE_FROM("AVIATOR_RULE_INFO");
        applyWhere(example, false);
        return SQL();
    }

    public String insertSelective(AviatorRuleInfo record) {
        BEGIN();
        INSERT_INTO("AVIATOR_RULE_INFO");
        
        if (record.getId() != null) {
            VALUES("ID", "#{id,jdbcType=DECIMAL}");
        }
        
        if (record.getRuleName() != null) {
            VALUES("RULE_NAME", "#{ruleName,jdbcType=VARCHAR}");
        }
        
        if (record.getRuleDescription() != null) {
            VALUES("RULE_DESCRIPTION", "#{ruleDescription,jdbcType=VARCHAR}");
        }
        
        if (record.getCreateBy() != null) {
            VALUES("CREATE_BY", "#{createBy,jdbcType=VARCHAR}");
        }
        
        if (record.getCreateTime() != null) {
            VALUES("CREATE_TIME", "#{createTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getUpdateTime() != null) {
            VALUES("UPDATE_TIME", "#{updateTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getUpdateBy() != null) {
            VALUES("UPDATE_BY", "#{updateBy,jdbcType=VARCHAR}");
        }
        
        if (record.getRuleType() != null) {
            VALUES("RULE_TYPE", "#{ruleType,jdbcType=VARCHAR}");
        }
        
        if (record.getRuleResult() != null) {
            VALUES("RULE_RESULT", "#{ruleResult,jdbcType=VARCHAR}");
        }
        
        if (record.getRulePriority() != null) {
            VALUES("RULE_PRIORITY", "#{rulePriority,jdbcType=VARCHAR}");
        }
        
        if (record.getOrganizationNumber() != null) {
            VALUES("ORGANIZATION_NUMBER", "#{organizationNumber,jdbcType=VARCHAR}");
        }
        
        if (record.getProductNumber() != null) {
            VALUES("PRODUCT_NUMBER", "#{productNumber,jdbcType=VARCHAR}");
        }
        
        if (record.getRuleConditions() != null) {
            VALUES("RULE_CONDITIONS", "#{ruleConditions,jdbcType=CLOB}");
        }
        
        return SQL();
    }

    public String selectByExampleWithBLOBs(AviatorRuleInfoExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("ID");
        } else {
            SELECT("ID");
        }
        SELECT("RULE_NAME");
        SELECT("RULE_DESCRIPTION");
        SELECT("CREATE_BY");
        SELECT("CREATE_TIME");
        SELECT("UPDATE_TIME");
        SELECT("UPDATE_BY");
        SELECT("RULE_TYPE");
        SELECT("RULE_RESULT");
        SELECT("RULE_PRIORITY");
        SELECT("ORGANIZATION_NUMBER");
        SELECT("PRODUCT_NUMBER");
        SELECT("RULE_CONDITIONS");
        FROM("AVIATOR_RULE_INFO");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    public String selectByExample(AviatorRuleInfoExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("ID");
        } else {
            SELECT("ID");
        }
        SELECT("RULE_NAME");
        SELECT("RULE_DESCRIPTION");
        SELECT("CREATE_BY");
        SELECT("CREATE_TIME");
        SELECT("UPDATE_TIME");
        SELECT("UPDATE_BY");
        SELECT("RULE_TYPE");
        SELECT("RULE_RESULT");
        SELECT("RULE_PRIORITY");
        SELECT("ORGANIZATION_NUMBER");
        SELECT("PRODUCT_NUMBER");
        FROM("AVIATOR_RULE_INFO");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    public String updateByExampleSelective(Map<String, Object> parameter) {
        AviatorRuleInfo record = (AviatorRuleInfo) parameter.get("record");
        AviatorRuleInfoExample example = (AviatorRuleInfoExample) parameter.get("example");
        
        BEGIN();
        UPDATE("AVIATOR_RULE_INFO");
        
        if (record.getId() != null) {
            SET("ID = #{record.id,jdbcType=DECIMAL}");
        }
        
        if (record.getRuleName() != null) {
            SET("RULE_NAME = #{record.ruleName,jdbcType=VARCHAR}");
        }
        
        if (record.getRuleDescription() != null) {
            SET("RULE_DESCRIPTION = #{record.ruleDescription,jdbcType=VARCHAR}");
        }
        
        if (record.getCreateBy() != null) {
            SET("CREATE_BY = #{record.createBy,jdbcType=VARCHAR}");
        }
        
        if (record.getCreateTime() != null) {
            SET("CREATE_TIME = #{record.createTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getUpdateTime() != null) {
            SET("UPDATE_TIME = #{record.updateTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getUpdateBy() != null) {
            SET("UPDATE_BY = #{record.updateBy,jdbcType=VARCHAR}");
        }
        
        if (record.getRuleType() != null) {
            SET("RULE_TYPE = #{record.ruleType,jdbcType=VARCHAR}");
        }
        
        if (record.getRuleResult() != null) {
            SET("RULE_RESULT = #{record.ruleResult,jdbcType=VARCHAR}");
        }
        
        if (record.getRulePriority() != null) {
            SET("RULE_PRIORITY = #{record.rulePriority,jdbcType=VARCHAR}");
        }
        
        if (record.getOrganizationNumber() != null) {
            SET("ORGANIZATION_NUMBER = #{record.organizationNumber,jdbcType=VARCHAR}");
        }
        
        if (record.getProductNumber() != null) {
            SET("PRODUCT_NUMBER = #{record.productNumber,jdbcType=VARCHAR}");
        }
        
        if (record.getRuleConditions() != null) {
            SET("RULE_CONDITIONS = #{record.ruleConditions,jdbcType=CLOB}");
        }
        
        applyWhere(example, true);
        return SQL();
    }

    public String updateByExampleWithBLOBs(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("AVIATOR_RULE_INFO");
        
        SET("ID = #{record.id,jdbcType=DECIMAL}");
        SET("RULE_NAME = #{record.ruleName,jdbcType=VARCHAR}");
        SET("RULE_DESCRIPTION = #{record.ruleDescription,jdbcType=VARCHAR}");
        SET("CREATE_BY = #{record.createBy,jdbcType=VARCHAR}");
        SET("CREATE_TIME = #{record.createTime,jdbcType=TIMESTAMP}");
        SET("UPDATE_TIME = #{record.updateTime,jdbcType=TIMESTAMP}");
        SET("UPDATE_BY = #{record.updateBy,jdbcType=VARCHAR}");
        SET("RULE_TYPE = #{record.ruleType,jdbcType=VARCHAR}");
        SET("RULE_RESULT = #{record.ruleResult,jdbcType=VARCHAR}");
        SET("RULE_PRIORITY = #{record.rulePriority,jdbcType=VARCHAR}");
        SET("ORGANIZATION_NUMBER = #{record.organizationNumber,jdbcType=VARCHAR}");
        SET("PRODUCT_NUMBER = #{record.productNumber,jdbcType=VARCHAR}");
        SET("RULE_CONDITIONS = #{record.ruleConditions,jdbcType=CLOB}");
        
        AviatorRuleInfoExample example = (AviatorRuleInfoExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    public String updateByExample(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("AVIATOR_RULE_INFO");
        
        SET("ID = #{record.id,jdbcType=DECIMAL}");
        SET("RULE_NAME = #{record.ruleName,jdbcType=VARCHAR}");
        SET("RULE_DESCRIPTION = #{record.ruleDescription,jdbcType=VARCHAR}");
        SET("CREATE_BY = #{record.createBy,jdbcType=VARCHAR}");
        SET("CREATE_TIME = #{record.createTime,jdbcType=TIMESTAMP}");
        SET("UPDATE_TIME = #{record.updateTime,jdbcType=TIMESTAMP}");
        SET("UPDATE_BY = #{record.updateBy,jdbcType=VARCHAR}");
        SET("RULE_TYPE = #{record.ruleType,jdbcType=VARCHAR}");
        SET("RULE_RESULT = #{record.ruleResult,jdbcType=VARCHAR}");
        SET("RULE_PRIORITY = #{record.rulePriority,jdbcType=VARCHAR}");
        SET("ORGANIZATION_NUMBER = #{record.organizationNumber,jdbcType=VARCHAR}");
        SET("PRODUCT_NUMBER = #{record.productNumber,jdbcType=VARCHAR}");
        
        AviatorRuleInfoExample example = (AviatorRuleInfoExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    public String updateByPrimaryKeySelective(AviatorRuleInfo record) {
        BEGIN();
        UPDATE("AVIATOR_RULE_INFO");
        
        if (record.getRuleName() != null) {
            SET("RULE_NAME = #{ruleName,jdbcType=VARCHAR}");
        }
        
        if (record.getRuleDescription() != null) {
            SET("RULE_DESCRIPTION = #{ruleDescription,jdbcType=VARCHAR}");
        }
        
        if (record.getCreateBy() != null) {
            SET("CREATE_BY = #{createBy,jdbcType=VARCHAR}");
        }
        
        if (record.getCreateTime() != null) {
            SET("CREATE_TIME = #{createTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getUpdateTime() != null) {
            SET("UPDATE_TIME = #{updateTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getUpdateBy() != null) {
            SET("UPDATE_BY = #{updateBy,jdbcType=VARCHAR}");
        }
        
        if (record.getRuleType() != null) {
            SET("RULE_TYPE = #{ruleType,jdbcType=VARCHAR}");
        }
        
        if (record.getRuleResult() != null) {
            SET("RULE_RESULT = #{ruleResult,jdbcType=VARCHAR}");
        }
        
        if (record.getRulePriority() != null) {
            SET("RULE_PRIORITY = #{rulePriority,jdbcType=VARCHAR}");
        }
        
        if (record.getOrganizationNumber() != null) {
            SET("ORGANIZATION_NUMBER = #{organizationNumber,jdbcType=VARCHAR}");
        }
        
        if (record.getProductNumber() != null) {
            SET("PRODUCT_NUMBER = #{productNumber,jdbcType=VARCHAR}");
        }
        
        if (record.getRuleConditions() != null) {
            SET("RULE_CONDITIONS = #{ruleConditions,jdbcType=CLOB}");
        }
        
        WHERE("ID = #{id,jdbcType=DECIMAL}");
        
        return SQL();
    }

    protected void applyWhere(AviatorRuleInfoExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            WHERE(sb.toString());
        }
    }
}