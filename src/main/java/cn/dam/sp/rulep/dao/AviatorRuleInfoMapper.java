package cn.dam.sp.rulep.dao;

import cn.dam.sp.rulep.bean.AviatorRuleInfo;
import cn.dam.sp.rulep.bean.AviatorRuleInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;


@Repository
public interface AviatorRuleInfoMapper {

    @SelectProvider(type=AviatorRuleInfoSqlProvider.class, method="countByExample")
    int countByExample(AviatorRuleInfoExample example);

    @DeleteProvider(type=AviatorRuleInfoSqlProvider.class, method="deleteByExample")
    int deleteByExample(AviatorRuleInfoExample example);

    @Delete({
        "delete from AVIATOR_RULE_INFO",
        "where ID = #{id,jdbcType=DECIMAL}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into AVIATOR_RULE_INFO (ID, RULE_NAME, ",
        "RULE_DESCRIPTION, CREATE_BY, ",
        "CREATE_TIME, UPDATE_TIME, ",
        "UPDATE_BY, RULE_TYPE, ",
        "RULE_RESULT, RULE_PRIORITY, ",
        "ORGANIZATION_NUMBER, PRODUCT_NUMBER, ",
        "RULE_CONDITIONS)",
        "values (#{id,jdbcType=DECIMAL}, #{ruleName,jdbcType=VARCHAR}, ",
        "#{ruleDescription,jdbcType=VARCHAR}, #{createBy,jdbcType=VARCHAR}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{updateBy,jdbcType=VARCHAR}, #{ruleType,jdbcType=VARCHAR}, ",
        "#{ruleResult,jdbcType=VARCHAR}, #{rulePriority,jdbcType=VARCHAR}, ",
        "#{organizationNumber,jdbcType=VARCHAR}, #{productNumber,jdbcType=VARCHAR}, ",
        "#{ruleConditions,jdbcType=CLOB})"
    })
    int insert(AviatorRuleInfo record);

    @InsertProvider(type=AviatorRuleInfoSqlProvider.class, method="insertSelective")
    int insertSelective(AviatorRuleInfo record);

    @SelectProvider(type=AviatorRuleInfoSqlProvider.class, method="selectByExampleWithBLOBs")
    @Results({
        @Result(column="ID", property="id", jdbcType=JdbcType.DECIMAL, id=true),
        @Result(column="RULE_NAME", property="ruleName", jdbcType=JdbcType.VARCHAR),
        @Result(column="RULE_DESCRIPTION", property="ruleDescription", jdbcType=JdbcType.VARCHAR),
        @Result(column="CREATE_BY", property="createBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="CREATE_TIME", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="UPDATE_TIME", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="UPDATE_BY", property="updateBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="RULE_TYPE", property="ruleType", jdbcType=JdbcType.VARCHAR),
        @Result(column="RULE_RESULT", property="ruleResult", jdbcType=JdbcType.VARCHAR),
        @Result(column="RULE_PRIORITY", property="rulePriority", jdbcType=JdbcType.VARCHAR),
        @Result(column="ORGANIZATION_NUMBER", property="organizationNumber", jdbcType=JdbcType.VARCHAR),
        @Result(column="PRODUCT_NUMBER", property="productNumber", jdbcType=JdbcType.VARCHAR),
        @Result(column="RULE_CONDITIONS", property="ruleConditions", jdbcType=JdbcType.CLOB)
    })
    List<AviatorRuleInfo> selectByExampleWithBLOBs(AviatorRuleInfoExample example);

    @SelectProvider(type=AviatorRuleInfoSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="ID", property="id", jdbcType=JdbcType.DECIMAL, id=true),
        @Result(column="RULE_NAME", property="ruleName", jdbcType=JdbcType.VARCHAR),
        @Result(column="RULE_DESCRIPTION", property="ruleDescription", jdbcType=JdbcType.VARCHAR),
        @Result(column="CREATE_BY", property="createBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="CREATE_TIME", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="UPDATE_TIME", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="UPDATE_BY", property="updateBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="RULE_TYPE", property="ruleType", jdbcType=JdbcType.VARCHAR),
        @Result(column="RULE_RESULT", property="ruleResult", jdbcType=JdbcType.VARCHAR),
        @Result(column="RULE_PRIORITY", property="rulePriority", jdbcType=JdbcType.VARCHAR),
        @Result(column="ORGANIZATION_NUMBER", property="organizationNumber", jdbcType=JdbcType.VARCHAR),
        @Result(column="PRODUCT_NUMBER", property="productNumber", jdbcType=JdbcType.VARCHAR)
    })
    List<AviatorRuleInfo> selectByExample(AviatorRuleInfoExample example);

    @Select({
        "select",
        "ID, RULE_NAME, RULE_DESCRIPTION, CREATE_BY, CREATE_TIME, UPDATE_TIME, UPDATE_BY, ",
        "RULE_TYPE, RULE_RESULT, RULE_PRIORITY, ORGANIZATION_NUMBER, PRODUCT_NUMBER, ",
        "RULE_CONDITIONS",
        "from AVIATOR_RULE_INFO",
        "where ID = #{id,jdbcType=DECIMAL}"
    })
    @Results({
        @Result(column="ID", property="id", jdbcType=JdbcType.DECIMAL, id=true),
        @Result(column="RULE_NAME", property="ruleName", jdbcType=JdbcType.VARCHAR),
        @Result(column="RULE_DESCRIPTION", property="ruleDescription", jdbcType=JdbcType.VARCHAR),
        @Result(column="CREATE_BY", property="createBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="CREATE_TIME", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="UPDATE_TIME", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="UPDATE_BY", property="updateBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="RULE_TYPE", property="ruleType", jdbcType=JdbcType.VARCHAR),
        @Result(column="RULE_RESULT", property="ruleResult", jdbcType=JdbcType.VARCHAR),
        @Result(column="RULE_PRIORITY", property="rulePriority", jdbcType=JdbcType.VARCHAR),
        @Result(column="ORGANIZATION_NUMBER", property="organizationNumber", jdbcType=JdbcType.VARCHAR),
        @Result(column="PRODUCT_NUMBER", property="productNumber", jdbcType=JdbcType.VARCHAR),
        @Result(column="RULE_CONDITIONS", property="ruleConditions", jdbcType=JdbcType.CLOB)
    })
    AviatorRuleInfo selectByPrimaryKey(Integer id);

    @UpdateProvider(type=AviatorRuleInfoSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") AviatorRuleInfo record, @Param("example") AviatorRuleInfoExample example);

    @UpdateProvider(type=AviatorRuleInfoSqlProvider.class, method="updateByExampleWithBLOBs")
    int updateByExampleWithBLOBs(@Param("record") AviatorRuleInfo record, @Param("example") AviatorRuleInfoExample example);

    @UpdateProvider(type=AviatorRuleInfoSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") AviatorRuleInfo record, @Param("example") AviatorRuleInfoExample example);

    @UpdateProvider(type=AviatorRuleInfoSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(AviatorRuleInfo record);

    @Update({
        "update AVIATOR_RULE_INFO",
        "set RULE_NAME = #{ruleName,jdbcType=VARCHAR},",
          "RULE_DESCRIPTION = #{ruleDescription,jdbcType=VARCHAR},",
          "CREATE_BY = #{createBy,jdbcType=VARCHAR},",
          "CREATE_TIME = #{createTime,jdbcType=TIMESTAMP},",
          "UPDATE_TIME = #{updateTime,jdbcType=TIMESTAMP},",
          "UPDATE_BY = #{updateBy,jdbcType=VARCHAR},",
          "RULE_TYPE = #{ruleType,jdbcType=VARCHAR},",
          "RULE_RESULT = #{ruleResult,jdbcType=VARCHAR},",
          "RULE_PRIORITY = #{rulePriority,jdbcType=VARCHAR},",
          "ORGANIZATION_NUMBER = #{organizationNumber,jdbcType=VARCHAR},",
          "PRODUCT_NUMBER = #{productNumber,jdbcType=VARCHAR},",
          "RULE_CONDITIONS = #{ruleConditions,jdbcType=CLOB}",
        "where ID = #{id,jdbcType=DECIMAL}"
    })
    int updateByPrimaryKeyWithBLOBs(AviatorRuleInfo record);

    @Update({
        "update AVIATOR_RULE_INFO",
        "set RULE_NAME = #{ruleName,jdbcType=VARCHAR},",
          "RULE_DESCRIPTION = #{ruleDescription,jdbcType=VARCHAR},",
          "CREATE_BY = #{createBy,jdbcType=VARCHAR},",
          "CREATE_TIME = #{createTime,jdbcType=TIMESTAMP},",
          "UPDATE_TIME = #{updateTime,jdbcType=TIMESTAMP},",
          "UPDATE_BY = #{updateBy,jdbcType=VARCHAR},",
          "RULE_TYPE = #{ruleType,jdbcType=VARCHAR},",
          "RULE_RESULT = #{ruleResult,jdbcType=VARCHAR},",
          "RULE_PRIORITY = #{rulePriority,jdbcType=VARCHAR},",
          "ORGANIZATION_NUMBER = #{organizationNumber,jdbcType=VARCHAR},",
          "PRODUCT_NUMBER = #{productNumber,jdbcType=VARCHAR}",
        "where ID = #{id,jdbcType=DECIMAL}"
    })
    int updateByPrimaryKey(AviatorRuleInfo record);
}