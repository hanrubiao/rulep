package cn.dam.sp.rulep.service.impl;

import cn.dam.sp.rulep.bean.AviatorRuleInfo;
import cn.dam.sp.rulep.dao.AviatorRuleInfoMapper;
import cn.dam.sp.rulep.service.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName RuleServiceImpl
 * @Description ruleService
 * @Author dam
 * @Date 2018/11/19 2:23 PM
 * Version 1.0
 **/
@Service
public class RuleServiceImpl implements RuleService {

    @Autowired
    private AviatorRuleInfoMapper aviatorRuleInfoMapper;

    /**
     * @Author dam
     * @Description      
     * @Date 4:19 PM 2018/11/19
     * @Param [] 
     * @Return RuleInfoBean
     **/     
    
    @Override
    public AviatorRuleInfo getRuleInfo(Integer ruleId) {
        return aviatorRuleInfoMapper.selectByPrimaryKey(ruleId);
    }


}
