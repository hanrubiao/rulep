package cn.dam.sp.rulep.service;

import cn.dam.sp.rulep.bean.AviatorRuleInfo;
import org.springframework.stereotype.Service;

@Service
public interface RuleService {

    AviatorRuleInfo getRuleInfo(Integer ruleId);
}
