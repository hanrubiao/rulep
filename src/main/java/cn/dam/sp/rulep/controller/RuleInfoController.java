package cn.dam.sp.rulep.controller;

import cn.dam.sp.rulep.bean.AviatorRuleInfo;
import cn.dam.sp.rulep.dao.AviatorRuleInfoMapper;
import cn.dam.sp.rulep.service.RuleService;
import cn.dam.sp.rulep.service.impl.RuleServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "rule")
@Slf4j
public class RuleInfoController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private RuleService ruleService;


    /**
     * @Author dam
     * @Description  根据ruleinfoid获取ruleinfo
     * @Date 5:00 PM 2018/11/19
     * @Param [ruleId]
     * @Return RuleInfoBean
     **/

    @GetMapping(value = "getRuleById/{ruleId}")
    public AviatorRuleInfo getRuleInfoById(@PathVariable("ruleId") Integer ruleId){
        AviatorRuleInfo ruleInfoBean = new AviatorRuleInfo();
        ruleInfoBean = ruleService.getRuleInfo(ruleId);
        return  ruleInfoBean;
    }

    /**
     * @Author dam
     * @Description
     * @Date 5:11 PM 2018/11/19
     * @Param []
     * @Return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     **/

    @GetMapping(value = "getRuleList")
    public List<Map<String,Object>> getRuleList(){

        List<Map<String,Object>> list = jdbcTemplate.queryForList
                ("select * from ANYTXN_V2_DEV.AVIATOR_RULE_INFO ");
        return list;

    }


}
