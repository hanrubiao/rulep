package cn.dam.sp.rulep.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;

@Configuration
@MapperScan(value = "cn.dam.sp.rulep.dao")
public class DruidConfiguration {


    /**
     * @Author dam
     * @Description      
     * @Date 4:19 PM 2018/11/19
     * @Param [] 
     * @Return javax.sql.DataSource
     **/     

    @Bean(initMethod = "init",destroyMethod = "close")
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource druidDataSource(){
        DruidDataSource druidDataSource = new DruidDataSource();
        return druidDataSource;

    }

    /**
     * @Author dam
     * @Description
     * @Date 4:42 PM 2018/11/19
     * @Param []
     * @Return org.springframework.boot.web.servlet.ServletRegistrationBean
     **/

    @Bean
    public ServletRegistrationBean druidStatViewServlet(){

        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean
                (new StatViewServlet(),"/druid/*");
        //添加初始化参数
        servletRegistrationBean.addInitParameter("allow","127.0.0.1");
        //登录查看信息的用户名和密码
        servletRegistrationBean.addInitParameter("loginUsername","admin");
        servletRegistrationBean.addInitParameter("loginPassword","123456");

        servletRegistrationBean.addInitParameter("resetEnable","false");
        return  servletRegistrationBean;

    }

    public FilterRegistrationBean druidStatFilter(){

        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new WebStatFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.addInitParameter("exclusions","*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");

        return  filterRegistrationBean;

    }




}
