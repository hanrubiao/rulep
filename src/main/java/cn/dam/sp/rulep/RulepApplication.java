package cn.dam.sp.rulep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RulepApplication {

    public static void main(String[] args) {
        SpringApplication.run(RulepApplication.class, args);
    }
}
